module.exports = {
    title: 'Hello Vuepress',
    description: 'Hello World',
    dest: 'public',
    base: '/vuepress-demo/',
    // 如果使用个人站点, 不需要配置 base
    // base: '/vue-blog/',
    head: [
        ['link', { rel: 'icon', href: '/books.png' }],
        // 在移动端, 搜索框在获得焦点时会放大, 并且在失去焦点后可以左右滚动, 这可以通过设置元来优化
        ['meta', { name: 'viewport', content: 'width=device-width,initial-scale=1,user-scalable=no' }]
    ],
    themeConfig: {
        logo: '/books.png',
        nav: [
            // { text: 'Home', link: '/' },
        ],
        sidebar: 'auto',
    }
}
